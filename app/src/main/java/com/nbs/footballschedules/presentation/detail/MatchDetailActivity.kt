package com.nbs.footballschedules.presentation.detail

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import com.nbs.footballschedules.R
import com.nbs.footballschedules.data.model.FootballSchedule
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_macth_detail.*

class MatchDetailActivity : AppCompatActivity(), MatchDetailContract.View {

    lateinit var presenter: MatchDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_macth_detail)

        supportActionBar.apply {
            title = "Match Detail"
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        val schedule = intent.getParcelableExtra<FootballSchedule>(scheduleBundleKey)

        presenter = MatchDetailPresenter(this)
        schedule.idHomeTeam?.let { presenter.getHomeTeamLogo(it) }
        schedule.idAwayTeam?.let { presenter.getAwayTeamLogo(it) }

        tvAwayTeam.text = schedule.strAwayTeam
        tvHomeTeam.text = schedule.strHomeTeam
        tvGoalHome.text = schedule.strHomeGoalDetails
        tvGoalAway.text = schedule.strAwayGoalDetails
        tvScore.text = "${schedule.intHomeScore} - ${schedule.intAwayScore}"
        tvShootsHome.text = schedule.intHomeShots
        tvShootsAway.text = schedule.intAwayShots
        lvKeeper.tvHomeLineUp.text = schedule.strHomeLineupGoalkeeper
        lvKeeper.tvAwayLineUp.text = schedule.strAwayLineupGoalkeeper
        lvDefender.tvHomeLineUp.text = schedule.strHomeLineupDefense
        lvDefender.tvAwayLineUp.text = schedule.strAwayLineupDefense
        lvMidfielder.tvHomeLineUp.text = schedule.strHomeLineupMidfield
        lvMidfielder.tvAwayLineUp.text = schedule.strAwayLineupMidfield
        lvAttacker.tvHomeLineUp.text = schedule.strHomeLineupForward
        lvAttacker.tvAwayLineUp.text = schedule.strAwayLineupForward
        lvSubtitute.tvHomeLineUp.text = schedule.strHomeLineupSubstitutes
        lvSubtitute.tvAwayLineUp.text = schedule.strAwayLineupSubstitutes

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val scheduleBundleKey: String = "scheduleKey"

        fun newIntent(context: Context, schedule: FootballSchedule?): Intent{
            val intent = Intent(context, MatchDetailActivity::class.java)
            intent.putExtra(scheduleBundleKey, schedule)

            return intent
        }
    }

    override fun showHomeTeamLogo(homeTeamLogo: String) {
        loadTeamLogo(homeTeamLogo, ivHomeTeam)
    }

    override fun showAwayTeamLogo(awayTeamLogo: String) {
        loadTeamLogo(awayTeamLogo, ivAwayTeam)
    }

    fun loadTeamLogo(teamLogo: String, imageView: ImageView){
        Picasso.get().load(teamLogo).into(imageView)
    }
}
