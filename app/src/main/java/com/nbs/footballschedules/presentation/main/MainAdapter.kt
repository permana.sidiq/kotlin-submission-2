package com.nbs.footballschedules.presentation.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nbs.footballschedules.R
import com.nbs.footballschedules.data.model.FootballSchedule
import kotlinx.android.synthetic.main.item_schedule.view.*
import java.text.SimpleDateFormat

class MainAdapter(var list: List<FootballSchedule>, private val onScheduleItemCallback: OnScheduleItemCallback): RecyclerView.Adapter<MainAdapter.MainViewholder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MainViewholder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_schedule, parent, false), onScheduleItemCallback)

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: MainViewholder, position: Int) {
        holder.bind(list[position])
    }

    class MainViewholder(itemView: View, val onScheduleItemCallback: OnScheduleItemCallback): RecyclerView.ViewHolder(itemView) {
        fun bind(item: FootballSchedule){
            with(item){
                itemView.tvDate.text = getFormattedDate(item.dateEvent)
                itemView.tvHomeTeam.text = item.strHomeTeam
                itemView.tvAwayTeam.text = item.strAwayTeam
                itemView.tvScore.text = "${item.intHomeScore} - ${item.intAwayScore}"

                itemView.cvSchedule.setOnClickListener {
                    onScheduleItemCallback.onItemScheduleClicked(item)
                }
            }
        }

        fun getFormattedDate(currentDate: String?): String{
            val strCurrentDate = currentDate
            var format = SimpleDateFormat("yyyy-MM-dd")
            val newDate = format.parse(strCurrentDate)

            format = SimpleDateFormat("EEE, MMM dd yyyy")
            return format.format(newDate)
        }
    }

    interface OnScheduleItemCallback{
        fun onItemScheduleClicked(schedule: FootballSchedule?)
    }
}