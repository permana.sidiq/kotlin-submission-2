package com.nbs.footballschedules.presentation.main

import com.nbs.footballschedules.data.model.FootballSchedule

interface MainContract {
    interface View{
        fun showLoading()

        fun hideLoading()

        fun showErrorMessage(message: String?)

        fun showData(list: List<FootballSchedule>?)
    }

    interface Presenter{
        fun requestData()
    }
}