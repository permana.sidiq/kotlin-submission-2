package com.nbs.footballschedules.presentation.custom

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.nbs.footballschedules.R

class LineUpView: BaseCustomView{

    lateinit var tvPosition: TextView

    lateinit var tvHomeLineUp: TextView

    lateinit var tvAwayLineUp: TextView

    private var lineupPosition: String? = null

    var homeLineUp: String? = null

    var awayLineUp: String? = null

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initialize(context, attrs)
    }

    override val layout: Int
        get() = R.layout.layout_line_up

    override fun bindView(view: View) {
        tvPosition = view.findViewById(R.id.tvLineUpPosition)
        tvAwayLineUp = view.findViewById(R.id.tvLineUpAway)
        tvHomeLineUp = view.findViewById(R.id.tvLineUpHome)

    }

    override fun initialize(context: Context, attrs: AttributeSet?) {
        super.initialize(context, attrs)
        parseAttrs(attrs)
        setLineUps()
    }

    private fun setLineUps(){
        tvHomeLineUp.text = homeLineUp
        tvAwayLineUp.text = awayLineUp
        tvPosition.text = lineupPosition
    }

    private fun parseAttrs(attrs: AttributeSet?) {
        if (attrs != null) {
            val typedArray = context
                    .obtainStyledAttributes(attrs, R.styleable.LineUp)
            try {
                lineupPosition = typedArray
                        .getString(R.styleable.LineUp_position)

                homeLineUp = typedArray
                        .getString(R.styleable.LineUp_home)

                awayLineUp = typedArray
                        .getString(R.styleable.LineUp_away)
            } finally {
                typedArray.recycle()
            }
        }
    }
}