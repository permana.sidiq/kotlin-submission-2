package com.nbs.footballschedules.presentation.detail

import android.util.Log
import com.nbs.footballschedules.data.callback.OnGetFootballTeamCallback
import com.nbs.footballschedules.data.model.FootballTeam
import com.nbs.footballschedules.domain.GetFootballTeamUseCase

class MatchDetailPresenter(val view: MatchDetailContract.View): MatchDetailContract.Presenter,
    OnGetFootballTeamCallback{

    private val getHomeTeamUseCase: GetFootballTeamUseCase = GetFootballTeamUseCase(true, this)

    private val getAwayTeamUseCase: GetFootballTeamUseCase = GetFootballTeamUseCase(false, this)

    override fun getHomeTeamLogo(teamId: String) {
        getHomeTeamUseCase.teamId = teamId
        getHomeTeamUseCase.request()
    }

    override fun getAwayTeamLogo(teamId: String) {
        getAwayTeamUseCase.teamId = teamId
        getAwayTeamUseCase.request()
    }

    override fun onGetFootballHomeTeamSuccess(team: FootballTeam?) {
        team?.strTeamBadge?.let { view.showHomeTeamLogo(it) }
    }

    override fun onGetFootballAwayTeamSuccess(team: FootballTeam?) {
        team?.strTeamBadge?.let { view.showAwayTeamLogo(it) }
    }

    override fun onGetFootballTeamFailed(messsage: String?) {
        Log.d("Request Team", messsage)
    }
}