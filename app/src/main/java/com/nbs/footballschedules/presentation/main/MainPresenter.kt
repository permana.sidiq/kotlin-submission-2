package com.nbs.footballschedules.presentation.main

import com.nbs.footballschedules.data.callback.OnGetScheduleCallback
import com.nbs.footballschedules.data.model.FootballSchedule
import com.nbs.footballschedules.domain.GetScheduleUseCase
import com.nbs.footballschedules.presentation.main.MainContract

class MainPresenter(val view: MainContract.View): MainContract.Presenter, OnGetScheduleCallback {

    var getScheduleUseCase: GetScheduleUseCase? = null

    init {
        getScheduleUseCase = GetScheduleUseCase(this)
    }

    override fun requestData() {
        view.showLoading()
        getScheduleUseCase?.request()
    }

    override fun onGetScheduleFailed(message: String?) {
        view.hideLoading()
        view.showErrorMessage(message)
    }

    override fun onGetScheduleSuccess(list: List<FootballSchedule>?) {
        view.hideLoading()
        view.showData(list)
    }
}