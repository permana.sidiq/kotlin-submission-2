package com.nbs.footballschedules.presentation.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.nbs.footballschedules.R
import com.nbs.footballschedules.data.model.FootballSchedule
import com.nbs.footballschedules.presentation.detail.MatchDetailActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.View, MainAdapter.OnScheduleItemCallback {

    private val presenter: MainPresenter = MainPresenter(this)

    lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvItems.setHasFixedSize(true)
        rvItems.layoutManager = LinearLayoutManager(this)

        adapter = MainAdapter(emptyList(), this)
        rvItems.adapter = adapter

        presenter.requestData()
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun showErrorMessage(message: String?) {
        toast(message)
    }

    fun toast(message: String?){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showData(listData: List<FootballSchedule>?) {
        listData?.let { adapter.list = it }
        adapter.notifyDataSetChanged()
    }

    override fun onItemScheduleClicked(schedule: FootballSchedule?) {
        startActivity(MatchDetailActivity.newIntent(this, schedule))
    }
}
