package com.nbs.footballschedules.presentation.detail

interface MatchDetailContract {
    interface View{
        fun showHomeTeamLogo(homeTeamLogo: String)

        fun showAwayTeamLogo(awayTeamLogo: String)
    }

    interface Presenter{
        fun getHomeTeamLogo(teamId: String)

        fun getAwayTeamLogo(teamId: String)
    }
}