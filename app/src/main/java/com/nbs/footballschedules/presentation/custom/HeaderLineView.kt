package com.nbs.footballschedules.presentation.custom

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.nbs.footballschedules.R

class HeaderLineView: BaseCustomView{

    lateinit var tvTitle: TextView

    private var title: String? = null

    constructor(context: Context) : super(context) {
        initialize(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initialize(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initialize(context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initialize(context, attrs)
    }

    override val layout: Int
        get() = R.layout.layout_header_line

    override fun bindView(view: View) {
        tvTitle = view.findViewById(R.id.tv_header_title)
    }

    override fun initialize(context: Context, attrs: AttributeSet?) {
        super.initialize(context, attrs)
        parseAttrs(attrs)
        setHeaderTitle()
    }

    fun setHeaderTitle(){
        tvTitle.text = title
    }

    private fun parseAttrs(attrs: AttributeSet?) {
        if (attrs != null) {
            val typedArray = context
                    .obtainStyledAttributes(attrs, R.styleable.HeaderLine)
            try {
                title = typedArray
                        .getString(R.styleable.HeaderLine_title)
            } finally {
                typedArray.recycle()
            }
        }
    }

}