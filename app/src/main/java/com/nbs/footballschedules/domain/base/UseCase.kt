package com.nbs.footballschedules.domain.base

import com.nbs.footballschedules.data.libs.BaseAsyncHttpClient

abstract class UseCase {
    var baseAsyncHttpClient: BaseAsyncHttpClient? = null

    init {
        baseAsyncHttpClient = BaseAsyncHttpClient()
    }

    abstract fun request()

    abstract fun onError(message: String?)

}