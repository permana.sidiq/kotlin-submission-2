package com.nbs.footballschedules.domain

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.nbs.footballschedules.data.callback.OnGetScheduleCallback
import com.nbs.footballschedules.data.libs.BaseAsyncHttpClient
import com.nbs.footballschedules.data.response.ScheduleResponse
import com.nbs.footballschedules.domain.base.UseCase

class GetScheduleUseCase(private val callback: OnGetScheduleCallback?): UseCase() {

    override fun request() {
        val leagueId = "4328"

        val params = listOf(Pair("id", leagueId))

        val url = baseAsyncHttpClient?.getAbsoluteUrl(BaseAsyncHttpClient.EVENT_PAST_LEAGUE)

        url?.httpGet(params)?.responseString{request, response, result ->
            when(result){
                is Result.Success -> {
                    val gson = Gson()

                    val scheduleResponse = gson.fromJson(result.get(), ScheduleResponse::class.java)

                    onReceivedResponse(scheduleResponse)
                }

                is Result.Failure -> {
                    onError(result.error.exception.message)
                }
            }
        }
    }

    override fun onError(message: String?) {
        callback?.onGetScheduleFailed(message)
    }

    private fun onReceivedResponse(scheduleResponse: ScheduleResponse){
        val list = scheduleResponse.list
        if (list != null){
            if (!list.isEmpty()){
                callback?.onGetScheduleSuccess(scheduleResponse.list)
            }else{
                onError("Empty Data")
            }
        }else{
            onError("No Data")
        }
    }

}