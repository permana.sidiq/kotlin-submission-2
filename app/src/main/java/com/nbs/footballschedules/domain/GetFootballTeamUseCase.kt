package com.nbs.footballschedules.domain

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.nbs.footballschedules.data.callback.OnGetFootballTeamCallback
import com.nbs.footballschedules.data.libs.BaseAsyncHttpClient
import com.nbs.footballschedules.data.model.FootballTeam
import com.nbs.footballschedules.data.response.FootballTeamResponse
import com.nbs.footballschedules.domain.base.UseCase

class GetFootballTeamUseCase(private val isHomeTeam: Boolean, private val onGetFootballTeamCallback: OnGetFootballTeamCallback): UseCase() {
    var teamId: String? = null

    override fun request() {
        val params = listOf(Pair("id", teamId))

        val url = baseAsyncHttpClient?.getAbsoluteUrl(BaseAsyncHttpClient.TEAM_BY_ID)

        url?.httpGet(params)?.responseString{request, response, result ->
            when(result){
                is Result.Success -> {
                    val gson = Gson()
                    val teamResponse =
                            gson.fromJson<FootballTeamResponse>(result.get(), FootballTeamResponse::class.java)

                    val list: List<FootballTeam>? = teamResponse.list

                    if (list != null){
                        if (isHomeTeam)
                            onGetFootballTeamCallback.onGetFootballHomeTeamSuccess(list[0])
                        else
                            onGetFootballTeamCallback.onGetFootballAwayTeamSuccess(list[0])
                    }else{
                        onError("Empty Data")
                    }
                }

                is Result.Failure -> {
                    onError(result.error.message)
                }
            }
        }
    }

    override fun onError(message: String?) {
        onGetFootballTeamCallback.onGetFootballTeamFailed(message)
    }
}