package com.nbs.footballschedules.data.response

import com.google.gson.annotations.SerializedName
import com.nbs.footballschedules.data.model.FootballSchedule

data class ScheduleResponse(
        @SerializedName("events")
        var list: List<FootballSchedule>? = null
)