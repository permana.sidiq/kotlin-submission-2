package com.nbs.footballschedules.data.model

import com.google.gson.annotations.SerializedName

data class FootballTeam(
        @SerializedName("idTeam")
        var idTeam: String? = null,

        @SerializedName("strTeam")
        var strTeam: String? = null,

        @SerializedName("strTeamBadge")
        var strTeamBadge: String? = null
)