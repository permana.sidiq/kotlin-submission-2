package com.nbs.footballschedules.data.callback

import com.nbs.footballschedules.data.model.FootballSchedule

interface OnGetScheduleCallback{
    fun onGetScheduleSuccess(list: List<FootballSchedule>?)

    fun onGetScheduleFailed(message: String?)
}