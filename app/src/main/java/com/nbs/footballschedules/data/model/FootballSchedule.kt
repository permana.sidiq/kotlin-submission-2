package com.nbs.footballschedules.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class FootballSchedule(
        @SerializedName("idEvent")
        var idEvent: String? = null,

        @SerializedName("strEvent")
        var strEvent: String? = null,

        @SerializedName("strFilename")
        var strFilename: String? = null,

        @SerializedName("strLeague")
        var strLeague: String? = null,

        @SerializedName("strHomeTeam")
        var strHomeTeam: String? = null,

        @SerializedName("strAwayTeam")
        var strAwayTeam: String? = null,

        @SerializedName("intHomeScore")
        var intHomeScore: String? = null,

        @SerializedName("intAwayScore")
        var intAwayScore: String? = null,

        @SerializedName("strHomeGoalDetails")
        var strHomeGoalDetails: String? = null,

        @SerializedName("strAwayGoalDetails")
        var strAwayGoalDetails: String? = null,

        @SerializedName("strHomeLineupGoalkeeper")
        var strHomeLineupGoalkeeper: String? = null,

        @SerializedName("strHomeLineupDefense")
        var strHomeLineupDefense: String? = null,

        @SerializedName("strHomeLineupMidfield")
        var strHomeLineupMidfield: String? = null,

        @SerializedName("strHomeLineupForward")
        var strHomeLineupForward: String? = null,

        @SerializedName("strHomeLineupSubstitutes")
        var strHomeLineupSubstitutes: String? = null,

        @SerializedName("strAwayLineupGoalkeeper")
        var strAwayLineupGoalkeeper: String? = null,

        @SerializedName("strAwayLineupDefense")
        var strAwayLineupDefense: String? = null,

        @SerializedName("strAwayLineupMidfield")
        var strAwayLineupMidfield: String? = null,

        @SerializedName("strAwayLineupForward")
        var strAwayLineupForward: String? = null,

        @SerializedName("strAwayLineupSubstitutes")
        var strAwayLineupSubstitutes: String? = null,

        @SerializedName("dateEvent")
        var dateEvent: String? = null,

        @SerializedName("idHomeTeam")
        var idHomeTeam: String? = null,

        @SerializedName("idAwayTeam")
        var idAwayTeam: String? = null,

        @SerializedName("intHomeShots")
        var intHomeShots: String? = null,

        @SerializedName("intAwayShots")
        var intAwayShots: String? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(idEvent)
        parcel.writeString(strEvent)
        parcel.writeString(strFilename)
        parcel.writeString(strLeague)
        parcel.writeString(strHomeTeam)
        parcel.writeString(strAwayTeam)
        parcel.writeString(intHomeScore)
        parcel.writeString(intAwayScore)
        parcel.writeString(strHomeGoalDetails)
        parcel.writeString(strAwayGoalDetails)
        parcel.writeString(strHomeLineupGoalkeeper)
        parcel.writeString(strHomeLineupDefense)
        parcel.writeString(strHomeLineupMidfield)
        parcel.writeString(strHomeLineupForward)
        parcel.writeString(strHomeLineupSubstitutes)
        parcel.writeString(strAwayLineupGoalkeeper)
        parcel.writeString(strAwayLineupDefense)
        parcel.writeString(strAwayLineupMidfield)
        parcel.writeString(strAwayLineupForward)
        parcel.writeString(strAwayLineupSubstitutes)
        parcel.writeString(dateEvent)
        parcel.writeString(idHomeTeam)
        parcel.writeString(idAwayTeam)
        parcel.writeString(intHomeShots)
        parcel.writeString(intAwayShots)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FootballSchedule> {
        override fun createFromParcel(parcel: Parcel): FootballSchedule {
            return FootballSchedule(parcel)
        }

        override fun newArray(size: Int): Array<FootballSchedule?> {
            return arrayOfNulls(size)
        }
    }
}