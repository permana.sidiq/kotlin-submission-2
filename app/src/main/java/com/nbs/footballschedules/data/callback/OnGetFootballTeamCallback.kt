package com.nbs.footballschedules.data.callback

import com.nbs.footballschedules.data.model.FootballTeam

interface OnGetFootballTeamCallback {
    fun onGetFootballHomeTeamSuccess(team: FootballTeam?)

    fun onGetFootballAwayTeamSuccess(team: FootballTeam?)

    fun onGetFootballTeamFailed(messsage: String?)
}