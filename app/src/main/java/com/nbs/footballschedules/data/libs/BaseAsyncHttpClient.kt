package com.nbs.footballschedules.data.libs

import com.nbs.footballschedules.BuildConfig

class BaseAsyncHttpClient{
    companion object ApiEndpoints{
        var EVENT_PAST_LEAGUE: String = "eventspastleague.php"

        var TEAM_BY_ID: String = "lookupteam.php"
    }

    fun getAbsoluteUrl(endpoint: String): String{
        return BuildConfig.BASE_URL + endpoint
    }
}