package com.nbs.footballschedules.data.response

import com.google.gson.annotations.SerializedName
import com.nbs.footballschedules.data.model.FootballTeam

data class FootballTeamResponse(
        @SerializedName("teams")
        var list: List<FootballTeam>? = null
)